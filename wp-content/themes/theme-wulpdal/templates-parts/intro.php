<?php
if( have_rows('sectie_1') ): 
while( have_rows('sectie_1') ): the_row(); 
$titel = get_sub_field('titel');
$subtitel = get_sub_field('subtitel');
$tekst = get_sub_field('tekst');
$afbeelding = get_sub_field('afbeelding');
$knop = get_sub_field('knop');
?>

    <section id="intro" data-animation="fade-in-up" data-hook=".7">
        <div class="container">
            <div id="content-image">
                <div class="row">
                    <div class="col center">
                        <div class="divider"></div>
                        <div class="title">
                            <h2><span><?php echo $titel; ?></span></h2>
                            <h3><?php echo $subtitel; ?></h3>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-md-6">
                        <div class="content">
                            <?php echo $tekst; ?>
                        </div>
                        <?php 
                        if( get_sub_field('knop') ):
                        $knop = get_sub_field('knop');
                        echo    '<a href="' . $knop[url] . '" class="btn-primary btn right" target="' . $knop[target] . '">' . $knop[title] . '</a>';
                        endif;
                        ?>
                    </div>
                    <div class="col-12 col-md-6">
                        <?php 
                        if( get_sub_field('afbeelding') ):
                        echo '<img src="' . $afbeelding . '" alt="">';
                        endif;
                        ?>
                    </div>
                </div>
            </div>


        </div>
    </section>
                                    
<?php
endwhile;
endif; 

if( have_rows('sectie_2') ): 
while( have_rows('sectie_2') ): the_row(); 
$titel = get_sub_field('titel');
$subtitel = get_sub_field('subtitel');
$tekst = get_sub_field('tekst');
$knop = get_sub_field('knop');
?>

    <section id="park" class="grey-bg">
        <div class="container">
            <div id="content-2-col" class="row">
                <div class="col-12">
                    <div class="wrap">
                        <div class="center">
                            <div class="title">
                                <h2><span><?php echo $titel; ?></span></h2>
                                <h3><?php echo $subtitel; ?></h3>
                            </div>
                        </div>
                        <div class="content">
                            <?php echo $tekst; ?>
                        </div>

                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col center">
                    <?php 
                    if( get_sub_field('knop') ):
                    $knop = get_sub_field('knop');
                    echo    '<a href="' . $knop[url] . '" class="btn-secondary btn white sfeerimpressie" target="' . $knop[target] . '">' . $knop[title] . '</a>';
                    endif;
                    ?>
                </div>
            </div>
        </div>
    </section>

<?php
endwhile;
endif; 
?>