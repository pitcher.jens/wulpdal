<?php 
$titel = get_sub_field('titel');
$tekst = get_sub_field('tekst');
$tooltip_active = get_sub_field('tooltip_active');
$tooltip_tekst = get_sub_field('tooltip_tekst');

echo '<div style="position: relative; float: left; width: 100%; border: solid 1px #000;">';
echo $titel;
echo $tekst;

?>

<?php 
//LOAD PACKAGES
$args=array(
'post_type' => 'pakketten',
'post_status' => 'publish',
'posts_per_page' => -1,
);
$my_query = null;
$my_query = new WP_Query($args);
if( $my_query->have_posts() ) {
    while ($my_query->have_posts()) : $my_query->the_post();

    $title = get_the_title();
    $actief = get_field('active');
    $link = get_permalink();
    echo '<a href="' . $link . '">Lees meer</a>';
    
    echo $title;
    echo  '<br>';
    
    // EIGENSCHAPPEN POSITIEF
    if( have_rows('inhoud_positief') ): 
        while( have_rows('inhoud_positief') ): the_row(); 

        $eigenschap = get_sub_field('eigenschap');
        echo    'Eigenschap: ';
        $term = get_term( $eigenschap, 'Eigenschappen' );
        echo $term->name;
        echo    '<br>';

        endwhile;
    endif;  
    // END EIGENSCHAPPEN POSITIEF
    
    
    // EIGENSCHAPPEN NEGATIEF
    if( have_rows('inhoud_negatief') ): 
        while( have_rows('inhoud_negatief') ): the_row(); 

        $eigenschap = get_sub_field('eigenschap');
        echo    'Eigenschap: ';
        echo    $term->name;
        echo    '<br>';

        endwhile;
    endif;  
    // END EIGENSCHAPPEN NEGATIEF
    
    endwhile;
} wp_reset_query();
?>

<?php
echo '</div>';
?>