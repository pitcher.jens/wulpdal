<?php get_header(); ?>
	
    <?php if (have_posts()) : ?>

    <main>

    
    <section class="sub_content" style="background-color: #e6e6e6;">		
		    <div class="container">
		    	<div class="row">
                    <div class="spacerVerical100"></div>
                    <div class="spacerVerical40"></div>
					<div class="col-xs-12 center">
						<div class="heading">
							<div class="circle two"></div>
							<div class="circle six"></div>
							<h2>Zoekresultaten</h2>		
							<div class="circle six"></div>
							<div class="circle two"></div>
						</div>			
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-10 col-sm-offset-1">
                        
                        
                        <div class="DIVresultLeft"><?php previous_posts_link('&laquo; Vorige resultaten') ?></div>
                            <div class="DIVresultRight"><?php next_posts_link('Volgende resultaten &raquo;') ?></div>

                                            <?php while (have_posts()) : the_post(); ?>

                                                <div class="DIVresultBlock">

                                                    <div class="DIVsearchLine"></div>
                                                    
                                                    <div class="post">
                                                    
                                                        <h3 id="post-<?php the_ID(); ?>" class="FONTsearch">
                                                            <?php the_title(); ?>
                                                        </h3>
                                                  
                                                        
                                                    <font class="FONTsearchResult">
                                                        
                                                        <?php the_excerpt(); ?>
                                                        
                                                        <?php if( get_field('blog_tekst') ): ?>
                                                        <?php 
                                                            $key_1_value = get_field('blog_tekst');
                                                            $key_1_value = strip_tags($key_1_value);
                                                            echo substr($key_1_value, 0, 250);
                                                        ?>...
                                                        <?php endif; ?>
                                                        
                                                        <?php if( get_field('vestiging_info_tekst') ): ?>
                                                        <?php 
                                                            $key_1_value = get_field('vestiging_info_tekst');
                                                            $key_1_value = strip_tags($key_1_value);
                                                            echo substr($key_1_value, 0, 250);
                                                        ?>...
                                                        <?php endif; ?>
                                                        
                                                        <?php if( get_field('lessen_tekst') ): ?>
                                                        <?php 
                                                            $key_1_value = get_field('lessen_tekst');
                                                            $key_1_value = strip_tags($key_1_value);
                                                            echo substr($key_1_value, 0, 250);
                                                        ?>...
                                                        <?php endif; ?>
                                                        
                                                        <?php if( get_field('instructeurs_tekst') ): ?>
                                                        <?php 
                                                            $key_1_value = get_field('instructeurs_tekst');
                                                            $key_1_value = strip_tags($key_1_value);
                                                            echo substr($key_1_value, 0, 250);
                                                        ?>...
                                                        <?php endif; ?>
                                                        
                                                    </font>

                                                        
                                                    <div class="DIVsearchLink">
                                                        <a href="<?php the_permalink(); ?>" class="text_link" rel="bookmark" title="Permanent Link to <?php the_title(); ?>">
                                                        Lees meer ›
                                                        </a>
                                                    </div>
   
                                                </div>
                                            
                                            </div>

                                            <?php endwhile; ?>
                                                    
                                            <div class="DIVsearchLine"></div>

                                            <div class="DIVresultLeft"><?php previous_posts_link('&laquo; Vorige resultaten') ?></div>
                                            <div class="DIVresultRight"><?php next_posts_link('Volgende resultaten &raquo;') ?></div>
                        
                        
                        
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 center">
                        
						
					</div>
				</div>
			</div>		
		</section>


<?php endif; ?>

				
			
	
<?php get_footer(); ?>
