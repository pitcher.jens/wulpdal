<?php
$view = get_sub_field('360_view');
?>
<section> 
    <div class="container">
        <div id="panorama-slider" class="row" data-animation="fade-in-up" data-hook=".7" style="opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0);">
            <div class="col col-sm-8 offset-sm-2">
                <?php echo $view; ?>
            </div>
        </div>
    </div>
</section>