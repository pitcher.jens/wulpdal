<?php
$titel = get_sub_field('titel');
$subtitel = get_sub_field('subtitel');
$knop = get_sub_field('knop');
$methode = get_sub_field('weergave');
$opmaak = get_sub_field('opmaak');
if ($methode == 'Toon alles') {
    $order = 'menu_order';
    $ids = '';
} else {
    $ids = get_sub_field('selectie', false, false);
    $order = 'post__in';
}
?>   

<?php 
if ($opmaak == 'optie1') {
    echo    '<section>';
} else if ($opmaak == 'optie2') {
    echo    '<section class="blue-bg">';
} else if ($opmaak == 'optie3') {
    echo    '<section class="blue-bg bg-image-bottom" style="background-image: url(' . get_template_directory_uri() . '/assets/images/wulpdal-zee_bg.jpg);">';
}
?>
        <div class="container">
            <?php if ($titel == '') { } else { ?>
            <div class="row">
                <div class="col center">
                    <div class="title">
                        <h2><span><?php echo $titel; ?></span></h2>
                        <h3><?php echo $subtitel; ?></h3>
                    </div>
                </div>
            </div>
            <?php } ?>
            <div id="woningen-grid" class="row" data-animation="fade-in-up" data-hook=".7">
                
                <?php 
                //LOAD PRODUCTEN
                $args=array(
                'post_type' => 'Woningen',
                'post_status' => 'publish',
                'posts_per_page' => -1,
                'post__in' => $ids,
                'order_by' => $order,
                'order' => 'ASC',
                );
                $my_query = null;
                $my_query = new WP_Query($args);
                if( $my_query->have_posts() ) {
                while ($my_query->have_posts()) : $my_query->the_post();

                $title = get_the_title();
                $tekst = custom_field_excerptII();
                $image = get_field('afbeelding');
                $actief = get_field('active');
                $link = get_permalink();
                ?>
                
                <div class="col-12 col-sm-6 col-xl-3">
                    <div class="woning">
                        <a href="<?php echo the_permalink(); ?>">
                            <div class="image" style="background-image: url('<?php echo $image['url']; ?>')"></div>
                            <div class="content center">
                                <p><?php echo $tekst; ?></p>
                                <div class="button btn">Bekijk <?php echo $title; ?></div>
                            </div>
                        </a>
                    </div>
                </div>
                    
                <?php endwhile;
                } wp_reset_query();
                ?>
                
            </div>

            <div class="row">
                <div class="col center">
                    <?php if ($knop == '') {} else { ?>
                    <a href="<?php echo $knop[url]; ?>" class="btn-secondary btn white" target="<?php echo $knop[target]; ?>"><?php echo $knop[title]; ?></a>
                    <?php } ?>
                </div>
            </div>
        </div>
    </section>