<?php get_header(); 

echo    '<main id="home">';

if (have_posts()) :
    while (have_posts()) : 
    the_post();

    get_template_part('builder', 'builder');

    endwhile;
endif;

echo    '</main>';

get_footer(); ?>