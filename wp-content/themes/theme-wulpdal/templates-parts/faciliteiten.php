<?php
$titel = get_sub_field('titel');
$subtitel = get_sub_field('subtitel');
$tekst = get_sub_field('tekst');
$ids = get_sub_field('faciliteiten', false, false);
$link = get_sub_field('link');
$panorama = get_sub_field('360');
if ($panorama == 1) {
    $html = get_sub_field('html');
} else {
    $afbeelding = get_sub_field('afbeelding');
}
?>

    <section id="faciliteiten" data-animation="fade-in-up" data-hook=".7">
        <div class="container">
            <div id="text-block" class="row">
                <div class="wrap center">
                    <div class="col">
                        <div class="divider"></div>
                        <div class="title">
                            <h2>
                                <span><?php echo $titel; ?></span>
                            </h2>
                            <h3>
                                <?php echo $subtitel; ?>
                            </h3>
                        </div>
                    </div>
                    <div class="col-12 col-lg-8 offset-lg-2">
                        <div class="content">
                            <?php echo  $tekst; ?>
                        </div>
                    </div>
                </div>
            </div>
            
            <div id="faciliteiten-overzicht" class="row">
                <div class="col-12 col-lg-6 faciliteiten">
                    <div class="row">
                        
                        <?php 
                        //LOAD PRODUCTEN
                        $args=array(
                        'post_type' => 'Faciliteiten',
                        'post_status' => 'publish',
                        'posts_per_page' => -1,
                        'post__in' => $ids,
                        'order_by' => 'post__in',
                        'order' => 'ASC',
                        );
                        $my_query = null;
                        $my_query = new WP_Query($args);
                        if( $my_query->have_posts() ) {
                        while ($my_query->have_posts()) : $my_query->the_post();

                        $omschrijving = get_field('omschrijving');
                        $iccon = get_field('iccon');
                        
                        echo    '<div class="col-6 col-sm-4 col-lg-6 faciliteit"><i class="wd-icon"><img src="' . $iccon . '" class="svg"></i><span>' . $omschrijving . '</span></div>';
                        
                        endwhile;
                        } wp_reset_query();
                        ?>
                        
                        <a href="<?php echo $link[url]; ?>" class="col-6 col-sm-4 col-lg-6 faciliteit" target="<?php echo $link[target]; ?>"><i class="wd-icon wd-faciliteiten"></i><span><?php echo $link[title]; ?></span></a>
                    </div>
                </div>
                
                <div id="panorama-slider" class="col-12 col-lg-6" style="background-image: url('<?php echo $afbeelding; ?>');">
                    <?php 
                    if ($panorama == 1) { 
                        echo  $html;
                    }
                    ?>
                </div>
            </div>
        </div>
    </section>