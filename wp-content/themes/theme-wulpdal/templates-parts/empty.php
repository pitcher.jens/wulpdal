<?php
$achtergrond = get_sub_field('achtergrond');
$hoogte = get_sub_field('hoogte');
if ($achtergrond == 'Wit') {
    $bg = '';
} else if ($achtergrond == 'Grijs') {
    $bg = 'grey-bg';  
} else if ($achtergrond == 'Donker grijs') {
    $bg = 'dark-grey-bg';
} else if ($achtergrond == 'Blauw') {
    $bg = 'blue-bg';  
}
echo    '<section class="' . $bg . '" style="height: ' . $hoogte . 'px;"></section>';
?>