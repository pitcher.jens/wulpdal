<?php
$titel = get_sub_field('titel');
$methode = get_sub_field('methode');

echo '<div style="position: relative; float: left; width: 100%; border: solid 1px #000;">';
echo $titel;

?>

<?php
echo $methode;
if ($methode == 'Selecteer blogs handmatig') {
    $ids = get_sub_field('selecteer_blogs', false, false);
}

//LOAD BLOGS
$args=array(
'post_type' => 'Post',
'post_status' => 'publish',
'posts_per_page' => 3,
'post__in' => $ids,
'orderby' => 'date',
'order' => 'DESC',
);
$my_query = null;
$my_query = new WP_Query($args);
if( $my_query->have_posts() ) {
    while ($my_query->have_posts()) : $my_query->the_post();
    
    $title = get_the_title();
    $tekst = get_the_content();
    $datum = get_the_date();
    $link = get_the_permalink();
    $link_tekst = get_field('link_tekst');
    $afbeelding = get_field('afbeelding');
    
    echo    $title;
    echo    $content;
    echo    $datum;
    echo    '<a href="' . $link . '">' . $link_tekst . '</a>';
    echo    $afbeelding;
    
    endwhile;
} wp_reset_query();
?>

<?php
echo '</div>';
?>