<?php
    if( have_rows('content_items') ):
        while ( have_rows('content_items') ) : the_row();

            if( get_row_layout() == 'content_banner' ):
                
                get_template_part('templates-parts/banner', 'banner');

            elseif( get_row_layout() == 'content_prices' ): 

                get_template_part('templates-parts/prices', 'prices');

            elseif( get_row_layout() == 'content_products' ): 

                get_template_part('templates-parts/products', 'products');

            elseif( get_row_layout() == 'content_reviews' ): 

                get_template_part('templates-parts/reviews', 'reviews');

            elseif( get_row_layout() == 'content_blogs' ): 

                get_template_part('templates-parts/blogs', 'blogs');
        
            elseif( get_row_layout() == 'content_tekst' ): 

                get_template_part('templates-parts/tekst', 'tekst');

            elseif( get_row_layout() == 'content_cta' ): 

                get_template_part('templates-parts/cta', 'cta');

            elseif( get_row_layout() == 'content_gallerij' ): 

                get_template_part('templates-parts/gallery', 'gallery');

            elseif( get_row_layout() == 'content_team' ): 

                get_template_part('templates-parts/team', 'team');

            elseif( get_row_layout() == 'content_contact' ): 

                get_template_part('templates-parts/contact', 'contact');

            elseif( get_row_layout() == 'content_faciliteiten' ): 

                get_template_part('templates-parts/faciliteiten', 'faciliteiten');

            elseif( get_row_layout() == 'google_maps' ): 

                get_template_part('templates-parts/googlemaps', 'googlemaps');

            elseif( get_row_layout() == 'content_360' ): 

                get_template_part('templates-parts/360', '360');

            elseif( get_row_layout() == 'content_news' ): 

                get_template_part('templates-parts/news', 'news');

            elseif( get_row_layout() == 'content_intro' ): 

                get_template_part('templates-parts/intro', 'intro');

            elseif( get_row_layout() == 'content_empty' ): 

                get_template_part('templates-parts/empty', 'empty');

            endif;

        endwhile;
    endif;
?>
