<?php
$indeling = get_sub_field('indeling');
$kolommen = get_sub_field('kolommen');

if( have_rows('tekst') ): 
while( have_rows('tekst') ): the_row(); 
    $uitlijning = get_sub_field('uitlijning');
    $knop_stijl = get_sub_field('knop_stijl');
    $knop_stijl_2 = get_sub_field('knop_stijl_2');

    if ($knop_stijl == 'Wit') {
        $btn = 'btn-secondary white';
    } else if ($knop_stijl == 'Zwart') {
        $btn = 'btn-secondary';
    } else if ($knop_stijl == 'Wit met zwarte omlijning') {
        $btn = 'btn-primary';
    } 

    if ($knop_stijl_2 == 'Wit') {
        $btn2 = 'btn-secondary white';
    } else if ($knop_stijl_2 == 'Zwart') {
        $btn2 = 'btn-secondary';
    } else if ($knop_stijl_2 == 'Wit met zwarte omlijning') {
        $btn2 = 'btn-primary';
    } 
endwhile;
endif; 


if( have_rows('opmaak') ): 
	while( have_rows('opmaak') ): the_row(); 
        $achtergrond = get_sub_field('achtergrond');
        $knop_kleur = get_sub_field('knop_kleur');
        if ($achtergrond == 'Wit') {
            $bg = '';
        } else if ($achtergrond == 'Grijs') {
            $bg = 'grey-bg';  
        } else if ($achtergrond == 'Donker grijs') {
            $bg = 'dark-grey-bg';
        } else if ($achtergrond == 'Blauw') {
            $bg = 'blue-bg';  
        }
	 endwhile;
endif; 

if( have_rows('afbeelding') ): 
    while( have_rows('afbeelding') ): the_row(); 
    $afbeelding = get_sub_field('item');
    endwhile;
endif; 
?>

<?php 
//INDELING: TEKST LINKS, AFBEELDING RECHTS
if ($indeling == 'Indeling 1') {

if( have_rows('tekst') ): 
	while( have_rows('tekst') ): the_row(); 
	
		$titel = get_sub_field('titel');
        $subtitel = get_sub_field('subtitel');
        $tekst = get_sub_field('tekst');
        $uitlijning = get_sub_field('uitlijning');
        $kolommen = get_sub_field('kolommen');
?>

<section  class="<?php echo  $bg; ?>">
    <div class="container">        
        <div class="spacing">
            <div id="content-image" class="row">
                <div class="col-12 col-lg-6">
                    <div class="title <?php if ($achtergrond == 'Blauw' || $achtergrond == 'Donker grijs') { echo 'white'; } ?>">
                        <h2><span><?php echo $titel; ?></span></h2>
                        <h3><?php echo $subtitel; ?></h3>
                    </div>
                    <div class="content <?php if ($achtergrond == 'Blauw' || $achtergrond == 'Donker grijs') { echo 'white'; } ?>">
                        <?php echo $tekst; ?>
                    </div>
                    <div class="button-wrapper">
                        <?php if( get_sub_field('knop') ):
                        $knop = get_sub_field('knop');
                        echo    '<a href="' . $knop[url] . '" class="' . $btn . ' btn" target="' . $knop[target] . '">' . $knop[title] . '</a>';
                        endif;
                        ?>
                        
                        <?php if( get_sub_field('knop_2') ):
                        $knop = get_sub_field('knop_2');
                        echo    '<a href="' . $knop[url] . '" class="' . $btn2 . ' btn" target="' . $knop[target] . '">' . $knop[title] . '</a>';
                        endif;
                        ?>
                        
                    </div>
                </div>
                <div class="col-12 col-lg-6" data-animation="fade-in-up" data-hook=".7" style="opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0);">
                    <?php 
                    if ($afbeelding == '') { } else {    
                    echo    '<img src="' . $afbeelding . '" alt="">'; 
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php
    endwhile;
endif; 
//INDELING: ENKEL TEKST
} else if ($indeling == 'Indeling 2') { 
if( have_rows('tekst') ): 
	while( have_rows('tekst') ): the_row(); 
	
		$titel = get_sub_field('titel');
        $subtitel = get_sub_field('subtitel');
        $tekst = get_sub_field('tekst');
        $uitlijning = get_sub_field('uitlijning');
        $kolommen = get_sub_field('kolommen');
?>
    
<section class="<?php echo $bg; ?>">
    <div class="container">        
        <div class="spacing">
            <div id="content-image" class="row">
                <div class="col-12 col-lg-6" data-animation="fade-in-up" data-hook=".7" style="opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0);">
                    <?php 
                    if ($afbeelding == '') { } else {    
                    echo    '<img src="' . $afbeelding . '" alt="">'; 
                    }
                    ?>
                </div>
                <div class="col-12 col-lg-6">
                    <div class="title <?php if ($achtergrond == 'Blauw' || $achtergrond == 'Donker grijs') { echo 'white'; } ?>">
                        <h2><span><?php echo $titel; ?></span></h2>
                        <h3><?php echo $subtitel; ?></h3>
                    </div>
                    <div class="content <?php if ($achtergrond == 'Blauw' || $achtergrond == 'Donker grijs') { echo 'white'; } ?>">
                        <?php echo $tekst; ?>
                    </div>
                    <div class="button-wrapper">
                        <?php if( get_sub_field('knop') ):
                        $knop = get_sub_field('knop');
                        echo    '<a href="' . $knop[url] . '" class="' . $btn . ' btn" target="' . $knop[target] . '">' . $knop[title] . '</a>';
                        endif;
                        ?>
                        
                        <?php if( get_sub_field('knop_2') ):
                        $knop = get_sub_field('knop_2');
                        echo    '<a href="' . $knop[url] . '" class="' . $btn2 . ' btn" target="' . $knop[target] . '">' . $knop[title] . '</a>';
                        endif;
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php
    endwhile;
endif; 

//INDELING: ENKEL TEKST
} else if ($indeling == 'Indeling 3') {

if( have_rows('tekst') ): 
	while( have_rows('tekst') ): the_row(); 
	
		$titel = get_sub_field('titel');
        $subtitel = get_sub_field('subtitel');
        $tekst = get_sub_field('tekst');
        $uitlijning = get_sub_field('uitlijning');
        $kolommen = get_sub_field('kolommen');

if ($kolommen == 1) {  		
?>
<section class="<?php echo  $bg; ?>">
    <div class="container">
        <div id="text-block" class="row">
            <div class="wrap center">
                <div class="col">
                    <?php if ($achtergrond == 'Blauw' || $achtergrond == 'Donker grijs') { } else { echo '<div class="divider"></div>'; } ?>
                    <div class="title <?php if ($achtergrond == 'Blauw' || $achtergrond == 'Donker grijs') { echo 'white'; } ?>">
                        <h2><span><?php echo $titel; ?></span></h2>
                        <h3><?php echo $subtitel; ?></h3>
                    </div>
                </div>
                <div class="col-12 col-lg-8 offset-lg-2">
                    <div class="content" <?php if ($achtergrond == 'Blauw' || $achtergrond == 'Donker grijs') { echo 'white'; } ?>>
                        <?php echo $tekst; ?>
                    </div>
                    <?php if( get_sub_field('knop') ):
                    $knop = get_sub_field('knop');
                    echo    '<a href="' . $knop[url] . '" class="' . $btn . ' btn" target="' . $knop[target] . '">' . $knop[title] . '</a>';
                    endif;
                    ?>
                    
                    <?php if( get_sub_field('knop_2') ):
                    $knop = get_sub_field('knop_2');
                    echo    '<a href="' . $knop[url] . '" class="' . $btn2 . ' btn" target="' . $knop[target] . '">' . $knop[title] . '</a>';
                    endif;
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php } else if ($kolommen == 2) { ?>

<section class="<?php echo  $bg; ?>">
    <div class="container">
        <div id="content-2-col" class="row">
            <div class="col-12 col-lg-10 offset-lg-1">
                <div class="wrap">
                    <div class="center">
                        <?php if ($achtergrond == 'Blauw' || $achtergrond == 'Donker grijs') { } else { echo '<div class="divider"></div>'; } ?>
                        <div class="title <?php if ($achtergrond == 'Blauw' || $achtergrond == 'Donker grijs') { echo 'white'; } ?>">
                            <h2 ><span><?php echo $titel; ?></span></h2>
                            <h3><?php echo $subtitel; ?></h3>
                        </div>
                    </div>
                    <div class="content <?php if ($achtergrond == 'Blauw' || $achtergrond == 'Donker grijs') { echo 'white'; } ?>">
                        <?php echo $tekst; ?>                                       
                    </div>
                    <div class="center">
                    <?php if( get_sub_field('knop') ):
                        $knop = get_sub_field('knop');
                        echo    '<a href="' . $knop[url] . '" class="' . $btn . ' btn" target="' . $knop[target] . '">' . $knop[title] . '</a>';
                        endif;
                    ?>
                        
                    <?php if( get_sub_field('knop_2') ):
                    $knop = get_sub_field('knop_2');
                    echo    '<a href="' . $knop[url] . '" class="' . $btn2 . ' btn" target="' . $knop[target] . '">' . $knop[title] . '</a>';
                    endif;
                    ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php
}
    endwhile;
endif;     
}
?>