<!DOCTYPE html>
<html lang="nl">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    
    <title><?php bloginfo('name'); ?> <?php wp_title(); ?></title>    
    
    <!-- FAVICON -->
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico" type="image/x-icon"/>
      
    <?php
    //LOAD HEADER SCRIPTS
    if( have_rows('scripts', 'option') ): 
        while( have_rows('scripts', 'option') ): the_row(); 
            $locatie = get_sub_field('locatie');
            if ($locatie == 'Header') {
                echo    get_sub_field('script');
            }
        endwhile;
    endif;  
    ?>
      
	<?php wp_head(); ?>
</head>

<body>
    
<?php
//LOAD HEADER SCRIPTS
if( have_rows('scripts', 'option') ): 
    while( have_rows('scripts', 'option') ): the_row(); 
        $locatie = get_sub_field('locatie');
        if ($locatie == 'Body') {
            echo    get_sub_field('script');
        }
    endwhile;
endif;  
?>
    
<?php 
$type = $_GET[type]; 
if ($type == '') {
    echo    '<style>.input_interesse {display: none !important;} </style>';
}
?>
    
<!-- Navigation-->
<nav id="main">
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <a href="<?php echo home_url(); ?>" class="logo">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/wulpdal-logo_small_white.svg" alt="">
                </a>
                <div class="hamburger">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
                
                <!-- MENU CUSTOM PITCHER -->
                <?php
                //LOAD MENU
                $post_object = get_field('menu_file', 'option');
                if( $post_object ): 
                    // override $post
                    $post = $post_object;
                    setup_postdata( $post ); 
                    $brochure = get_permalink();
                    wp_reset_postdata();
                endif;
                
                $curlink = home_url( $wp->request );
                $curlink = $curlink . '/';
                
                $download_tekst = get_field('menu_download', 'option');
                $download_file = get_field('menu_file', 'option');
                
                if( have_rows('menu_items', 'option') ): 
                    echo    '<ul>';
                    while( have_rows('menu_items', 'option') ): the_row(); 
                        $tekst = get_sub_field('menu_tekst');
                        $link = get_sub_field('menu_link');
                        if ($link[url] == $curlink) {
                            $active = 'class="active"';
                        } else {
                            $active = '';
                        }
                
                        echo    '<li ' . $active . '><a href="' . $link[url] . '" target="' . $link[target] . '"><span>' . $tekst . '</span></a></li>';
                    endwhile;
                
                    if ($download_tekst == '') { } else {
                        echo    '<li class="download-brochure"><a href="' . $brochure . '"><i class="wd-icon wd-brochure"></i>' . $download_tekst . '</a></li>';
                    }
                
                    echo    '</ul>';
                endif;  
                ?>
                <!-- END MENU CUSTOM PITCHER -->
                
            </div>
        </div>
    </div>
</nav>