<?php 
// Config 1: Remove editor on selected pages

add_action( 'admin_init', 'hide_editor' );
function hide_editor() {
    $post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
    if( !isset( $post_id ) ) return;
    $pagename = get_the_title($post_id);
    if($pagename == 'my-page'){
    remove_post_type_support('page', 'editor');
  }
    
  // Hide the editor on a page with a specific page template
  $template_file = get_post_meta($post_id, '_wp_page_template', true);
  if($template_file == 'home.php'){
    remove_post_type_support('page', 'editor');
  } if($template_file == 'page.php'){
    remove_post_type_support('page', 'editor');
  } 
}

function remove_editor() {
  remove_post_type_support('page', 'editor');
}
add_action('admin_init', 'remove_editor');
// End


// Config 2: Hide reactions

function thmlv_disable_comments_post_types_support() {
	$post_types = get_post_types();
 	foreach ($post_types as $post_type) {
 		if(post_type_supports($post_type, 'comments')) {
 			remove_post_type_support($post_type, 'comments');
 			remove_post_type_support($post_type, 'trackbacks');
 		}
 	}
}
add_action('admin_init', 'thmlv_disable_comments_post_types_support');

function thmlv_disable_comments_status() {
	return false;
}
add_filter('comments_open', 'thmlv_disable_comments_status', 20, 2);
add_filter('pings_open', 'thmlv_disable_comments_status', 20, 2);

function thmlv_disable_comments_hide_existing_comments($comments) {
	$comments = array();
	return $comments;
}
add_filter('comments_array', 'thmlv_disable_comments_hide_existing_comments', 10, 2);

function thmlv_disable_comments_admin_menu() {
	remove_menu_page('edit-comments.php');
}
add_action('admin_menu', 'thmlv_disable_comments_admin_menu');

function thmlv_disable_comments_admin_menu_redirect() {
	global $pagenow;
	if ($pagenow === 'edit-comments.php') {
		wp_redirect(admin_url()); exit;
	}
}
add_action('admin_init', 'thmlv_disable_comments_admin_menu_redirect');

function thmlv_disable_comments_dashboard() {
	remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');
}
add_action('admin_init', 'thmlv_disable_comments_dashboard');

function thmlv_disable_comments_admin_bar() {
	if (is_admin_bar_showing()) {
		remove_action('admin_bar_menu', 'wp_admin_bar_comments_menu', 60);
	}
}
add_action('init', 'thmlv_disable_comments_admin_bar');


function remove_menus(){
    remove_menu_page( 'edit-comments.php' );
}
add_action( 'admin_menu', 'remove_menus' );

// End


// Config 3: Remove images

add_action('init', 'myprefix_remove_tax');
function myprefix_remove_tax() {
    register_taxonomy('post_tag', array());
}

function is_blog () {
    return ( is_archive() || is_author() || is_category() || is_home() || is_single() || is_tag()) && 'post' == get_post_type();
}

function wpdocs_setup_theme() {
    add_image_size( 'blog', 500, 500, true );
}

function my_remove_wp_seo_meta_box() {
    remove_meta_box('wpseo_meta', 'footer', 'normal');
    remove_meta_box('wpseo_meta', 'scripts', 'normal');
}
add_action('add_meta_boxes', 'my_remove_wp_seo_meta_box', 100);

// End


// Config 4: Remove dashboard widgets

function remove_dashboard_widgets() {
	global $wp_meta_boxes;

	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_drafts']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
}

add_action('wp_dashboard_setup', 'remove_dashboard_widgets' );

// End


// Config 5: Edit dashboard buttons
add_action( 'admin_init', 'my_remove_menu_pages' );
function my_remove_menu_pages() {
    
$user_id = get_current_user_id();

    // Dashboard
    if( have_rows('admin_dashboard', 'options') ): 
        while( have_rows('admin_dashboard', 'options') ): the_row(); 
            $super_admin = get_sub_field('super_admin');
            $user = get_sub_field('user');
            if ($user_id == 1) {
                if ($super_admin == '') {
                    remove_menu_page('index.php'); // Media
                } 
            } else {
                if ($user == '') {
                    remove_menu_page('index.php'); // Media
                } 
            }
        endwhile;
    endif;

    // Pagina's
    if( have_rows('admin_pages', 'option') ): 
        while( have_rows('admin_pages', 'option') ): the_row(); 
            $super_admin = get_sub_field('super_admin');
            $user = get_sub_field('user');
            if ($user_id == 1) {
                if ($super_admin == '') {
                    remove_menu_page('edit.php?post_type=page'); // Pages
                } 
            } else {
                if ($user == '') {
                    remove_menu_page('edit.php?post_type=page'); // Pages
                } 
            }
        endwhile;
    endif;

    // Media
    if( have_rows('admin_media', 'option') ): 
        while( have_rows('admin_media', 'option') ): the_row(); 
            $super_admin = get_field('super_admin');
            $user = get_field('user');
            if ($user_id == 1) {
                if ($super_admin == '') {
                    remove_menu_page('upload.php'); // Media
                } 
            } else {
                if ($user == '') {
                    remove_menu_page('upload.php'); // Media
                } 
            }
        endwhile;
    endif;

    // Weergave
    if( have_rows('admin_weergave', 'option') ): 
        while( have_rows('admin_weergave', 'option') ): the_row(); 
            $super_admin = get_sub_field('super_admin');
            $user = get_sub_field('user');
            if ($user_id == 1) {
                if ($super_admin == '') {
                    remove_menu_page('themes.php'); // Appearance
                } 
            } else {
                if ($user == '') {
                    remove_menu_page('themes.php'); // Appearance
                } 
            }
        endwhile;
    endif;

    // Plugins
    if( have_rows('admin_plugins', 'option') ): 
        while( have_rows('admin_plugins', 'option') ): the_row(); 
            $super_admin = get_sub_field('super_admin');
            $user = get_sub_field('user');
            if ($user_id == 1) {
                if ($super_admin == '') {
                    remove_menu_page('plugins.php'); // Plugins
                } 
            } else {
                if ($user == '') {
                    remove_menu_page('plugins.php'); // Plugins
                } 
            }
        endwhile;
    endif;

    // Gebruikers
    if( have_rows('admin_gebruikers', 'option') ): 
        while( have_rows('admin_gebruikers', 'option') ): the_row(); 
            $super_admin = get_sub_field('super_admin');
            $user = get_sub_field('user');
            if ($user_id == 1) {
                if ($super_admin == '') {
                    remove_menu_page('users.php'); // Users
                } 
            } else {
                if ($user == '') {
                    remove_menu_page('users.php'); // Users
                } 
            }
        endwhile;
    endif;

    // Instellingen
    if( have_rows('admin_instellingen', 'option') ): 
        while( have_rows('admin_instellingen', 'option') ): the_row(); 
            $super_admin = get_sub_field('super_admin');
            $user = get_sub_field('user');
            if ($user_id == 1) {
                if ($super_admin == '') {
                    remove_menu_page('options-general.php'); // Settings
                } 
            } else {
                if ($user == '') {
                    remove_menu_page('options-general.php'); // Settings
                } 
            }
        endwhile;
    endif;

    // ACF Extra Velden
    if( have_rows('admin_acf', 'option') ): 
        while( have_rows('admin_acf', 'option') ): the_row(); 
            $super_admin = get_sub_field('super_admin');
            $user = get_sub_field('user');
            if ($user_id == 1) {
                if ($super_admin == '') {
                    remove_menu_page('edit.php?post_type=acf-field-group'); // ACF Fields
                } 
            } else {
                if ($user == '') {
                    remove_menu_page('edit.php?post_type=acf-field-group'); // ACF Fields
                } 
            }
        endwhile;
    endif;

    // Contactform 7
    if( have_rows('admin_cf7', 'option') ): 
        while( have_rows('admin_cf7', 'option') ): the_row(); 
            $super_admin = get_sub_field('super_admin');
            $user = get_sub_field('user');
            if ($user_id == 1) {
                if ($super_admin == '') {
                    remove_menu_page( 'wpcf7' ); // Contactform 7
                } 
            } else {
                if ($user == '') {
                    remove_menu_page( 'wpcf7' ); // Contactform 7
                } 
            }
        endwhile;
    endif;

    // Options
    if( have_rows('admin_options', 'option') ): 
        while( have_rows('admin_options', 'option') ): the_row(); 
            $super_admin = get_sub_field('super_admin');
            $user = get_sub_field('user');
            if ($user_id == 1) {
                if ($super_admin == '') {
                    remove_menu_page( 'acf-options' ); // Options 
                } 
            } else {
                if ($user == '') {
                    remove_menu_page( 'acf-options' ); // Options 
                } 
            }
        endwhile;
    endif;
    
    // WP Security
    if( have_rows('admin_wpsecurity', 'option') ): 
        while( have_rows('admin_wpsecurity', 'option') ): the_row(); 
            $super_admin = get_sub_field('super_admin');
            $user = get_sub_field('user');
            if ($user_id == 1) {
                if ($super_admin == '') {
                    remove_menu_page( 'aiowpsec' ); // Options 
                } 
            } else {
                if ($user == '') {
                    remove_menu_page( 'aiowpsec' ); // Options 
                } 
            }
        endwhile;
    endif;
    
    // Extra
    if( have_rows('admin_extra', 'option') ): 
        while( have_rows('admin_extra', 'option') ): the_row(); 
            $super_admin = get_sub_field('super_admin');
            $user = get_sub_field('user');
            if ($user_id == 1) {
                if ($super_admin == '') {
                    remove_menu_page( 'tools.php' ); // Options 
                } 
            } else {
                if ($user == '') {
                    remove_menu_page( 'tools.php' ); // Options 
                } 
            }
        endwhile;
    endif;
 
    
    if(get_field('module_blogs', 'option') == '') {
        remove_menu_page( 'edit.php' ); // Options 
    }
    
    remove_menu_page('cfdb7-list.php'); 
    remove_menu_page('jvcf7_settings_page'); 
    
}

if( have_rows('admin_updates', 'option') ): 
    while( have_rows('admin_updates', 'option') ): the_row(); 
        $super_admin = get_sub_field('super_admin');
        $user = get_sub_field('user');
        if ($user_id == 1) {
            if ($super_admin == '') {  
                function edit_admin_menus() {  
                global $submenu;  
                unset($submenu['index.php'][10]);
                return $submenu;
                }  
                add_action( 'admin_menu', 'edit_admin_menus' ); 
            } 
        } else {
            if ($user == '') {
                function edit_admin_menus() {  
                global $submenu;  
                unset($submenu['index.php'][10]);
                return $submenu;
                }  
                add_action( 'admin_menu', 'edit_admin_menus' ); 
            } 
        }
    endwhile;
endif;



// Config 6: Rename blog module
function change_post_menu_label() {
    global $menu;
    global $submenu;
    $menu[5][0] = 'Blog';
    $submenu['edit.php'][5][0] = 'Blog';
    $submenu['edit.php'][10][0] = 'Nieuw bericht';
    echo '';
}

function change_post_object_label() {
        global $wp_post_types;
        $labels = &$wp_post_types['post']->labels;
        $labels->name = 'Berichten';
        $labels->singular_name = 'Bericht';
        $labels->add_new = 'Nieuw bericht';
        $labels->add_new_item = 'Nieuw bericht';
        $labels->edit_item = 'Bewerk bericht';
        $labels->new_item = 'Bericht';
        $labels->view_item = 'Bekijk bericht';
        $labels->search_items = 'Zoek bericht';
        $labels->not_found = 'Geen berichten gevonden';
        $labels->not_found_in_trash = 'Geen bericht gevonden';
    }
    add_action( 'init', 'change_post_object_label' );
    add_action( 'admin_menu', 'change_post_menu_label' );


// Change menu order
function custom_menu_order($menu_ord) {
    if (!$menu_ord) return true;
     
    return array(
        'index.php', // Dashboard
        'separator1', // First separator
        'edit.php?post_type=page', // Pages
        'edit.php', // Posts
        'edit.php?post_type=woningen',
        'edit.php?post_type=faciliteiten',
        'edit.php?post_type=faciliteiten',
        'edit.php?post_type=inventaris',
        'edit.php?post_type=nieuws',
        'edit.php?post_type=downloads',
        'edit.php?post_type=mapplic_map',
        'admin.php?page=wpcf7',
        'admin.php?page=cfdb7-list.php',
    );
}
add_filter('custom_menu_order', 'custom_menu_order'); // Activate custom_menu_order
add_filter('menu_order', 'custom_menu_order');

add_action( 'admin_menu', 'linked_url' );
    function linked_url() {
    add_menu_page( 'linked_url', 'Inzendingen', 'read', 'my_slug', '', 'dashicons-portfolio', 1 );
    }

    add_action( 'admin_menu' , 'linkedurl_function' );
    function linkedurl_function() {
    global $menu;
    $menu[1][2] = "https://www.vakantieparkwulpdal.nl/wp-admin/admin.php?page=cfdb7-list.php";
}


// Config 7: Change length of excerpt
function wpdocs_custom_excerpt_length( $length ) {
    return 20;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );


// Config 8: Custom Dashboard Box
add_action('wp_dashboard_setup', 'my_custom_dashboard_widgets');
  
function my_custom_dashboard_widgets() {
global $wp_meta_boxes;
 
wp_add_dashboard_widget('custom_help_widget', 'Welkom', 'custom_dashboard_help');
}
 
function custom_dashboard_help() {
echo '<center><img src="' . get_template_directory_uri() . '/theme-files/admin/ptchr.png"><p>Welkom in het admin systeem van de website ' . get_site_url() . '.<br><br>Heeft u vragen of hulp nodig?<br> Neem dan contact op met <a href = "http://www.ptchr.nl/" target="_blank">Pitcher</a>.<br><br></p></center>';
}

remove_action('welcome_panel', 'wp_welcome_panel');
?>