<?php
$titel = get_sub_field('titel');
$subtitel = get_sub_field('subtitel');
$tekst_actief = get_sub_field('tekst_actief');
$tekst = get_sub_field('tekst');
if( get_sub_field('link') ):
    $knop = get_sub_field('link');
endif;
?>

<?php 
$images = get_sub_field('afbeeldingen');
$total = 0;
if( $images ):
foreach( $images as $image ):
$total++;
endforeach;              
endif; 
?>

<?php
$achtergrond = get_sub_field('achtergrond');
if ($achtergrond == 'Wit') {
    $bg = '';
} else if ($achtergrond == 'Grijs') {
    $bg = 'grey-bg';  
} else if ($achtergrond == 'Donker grijs') {
    $bg = 'dark-grey-bg';
} else if ($achtergrond == 'Blauw') {
    $bg = 'blue-bg';  
}

$counter = 1;
if( have_rows('afbeeldingen') ): ?>
    <section class="<?php echo $bg; ?>">
        <div class="container">
            
            <?php if ($titel == '') { } else { 
                if ($tekst_actief == 1) {
            ?>
            
                <div id="text-block" class="row">
                <div class="wrap center">
                    <div class="col">
                        <div class="divider"></div>
                        <div class="title">
                            <h2><?php echo $titel; ?></h2>
                        </div>
                    </div>
                    <div class="col-12 col-lg-8 offset-lg-2">
                        <div class="content">
                            <?php echo $tekst; ?>
                        </div>
                    </div>
                </div>
            </div>
            
            <?php
            } else {
            ?>
            
            <div class="col-12 col-lg-10 offset-lg-1">
                <div class="wrap">
                    <div class="center">
                        <div class="divider"></div>                        
                        <div class="title no-margin">
                            <h2><span><?php echo $titel; ?></span></h2>
                            <h3><?php echo $subtitel; ?></h3>
                        </div>
                    </div>
                </div>
            </div>
        
            <?php
                }
            }   
            ?>
            
            <div id="grid-gallery" class="row light-gallery"  data-animation="fade-in-up" data-hook=".7">
                <div class="col-12 col-xl-10 offset-xl-1">
                    <div class="row">
                        <div class="col">
                            <div class="owl-carousel gallery-carousel">
                                
                            <?php
                            while( have_rows('afbeeldingen') ): the_row(); 
                            $image = get_sub_field('afbeelding');
                            if ($counter == 1) {
                                $class = 'h2';
                            } else if ($counter == 2) {
                                $class = 'w2';
                            } else if ($counter == 3) {
                                $class = 'w2';
                            }
                            ?>
                            
                            <div class="grid-item light-item <?php echo $class; ?>" data-src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>">
                                <img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" />
                                <div class="img" style="background-image: url('<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>"></div>
                            </div>
                            <?php
                            $counter++;
                            endwhile;
                            ?>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php
            if( get_sub_field('knop') ):
            $knop = get_sub_field('knop'); 
            ?>

            <div class="col-12 col-lg-10 offset-lg-1">
                <div class="wrap">
                    <div class="center">
                    <?php echo '<a href="' . $knop[url] . '" class="btn-secondary btn" target="' . $knop[target] . '">' . $knop[title] . '</a>'; ?>      
                    </div>
                </div>
            </div>

            <?php endif; ?>
        
        </div>
    </section>
                    
<?php endif; ?>