<?php get_header(); ?>

<?php
if (have_posts()) :
 while (have_posts()) : 
the_post(); ?>

<?php
$title = get_the_title();
$tekst = get_the_content();
$datum = get_the_date();
$link = get_the_permalink();
$link_tekst = get_field('link_tekst');
$afbeelding = get_field('afbeelding');
?>

<div style="position: relative; float: left; width: 100%; border: solid 1px #000;">

<?php
echo    $title;
echo    $content;
echo    $datum;
echo    $afbeelding;
?>	   	
    
</div>
    
 <?php
 endwhile;
endif;
?>

<?php get_footer(); ?>