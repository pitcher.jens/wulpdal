<?php get_header();

if (have_posts()) :
while (have_posts()) : 
the_post(); 

$title = get_the_title();
$tekst = get_field('tekst');
$date =  get_the_date();
$afbeelding = get_field('afbeelding');
$curid = get_the_ID();
?>

<main id="home">
    
    <section class="nieuws-image">
        <div class="container">
            <div class="col-12 col-lg-10 offset-lg-1 no-padding">
                <div class="wrap">
                    <div class="center">
                        <div class="divider"></div>
                        <div class="title">
                            <h2><?php echo $title; ?></h2>
                            <br>
                            <?php echo $date; ?>
                        </div>
                    </div>
                    <img src=" <?php echo $afbeelding; ?>">
                </div>
            </div>
        </div>
    </section>
    
   <section class="grey-bg content-overlap">
       <div class="container">
       
            <div id="content-2-col" class="row">
                <div class="col-12 col-lg-10 offset-lg-1">
                    <div class="wrap">                            
                        <div class="content">
                        <?php echo $tekst; ?>
                        </div>
                    </div>
                </div>
            </div>
           
           <div class="row">
                <div class="col-12 col-lg-10 offset-lg-1">
                    <a href="javascript: history.go(-1)" class="btn-primary btn"><i class="fa fa-chevron-left" aria-hidden="true"></i> Terug</a>
                </div>
           </div>
                                          
        </div>
    </section>
    
    <section id="woning-carousel">
        <div class="container">
            
             <div id="nieuws-grid" class="row" >
                 <div class="col-12 col-xl-10 offset-xl-1">
                     <div class="row">
                         <div class="col">
                             <div class="divider"></div>
                             <div class="title center">
                                 <h2>Bekijk ook deze berichten</h2>
                             </div>
                         </div>
                     </div>
                     <div class="row" data-animation="fade-in-up" data-hook=".7">
                         
                         <?php 
                        //LOAD PRODUCTEN
                        $args=array(
                        'post_type' => 'Nieuws',
                        'post_status' => 'publish',
                        'posts_per_page' => 3,
                        'orderby' => 'date',
                        'order' => 'DESC',
                        'post__not_in' => array($curid),
                        );
                        $my_query = null;
                        $my_query = new WP_Query($args);
                        if( $my_query->have_posts() ) {
                        while ($my_query->have_posts()) : $my_query->the_post();

                            $title = get_the_title();
                            $afbeelding = get_field('afbeelding');
                            $tekst = custom_field_excerpt();
                            $link = get_permalink();
                            
                            echo    '<div class="col-md-6 col-lg-4">';
                            echo    '<div class="nieuws smallshadow">';
                            echo    '<a href="' . $link . '">';
                            echo    '<div class="image" style="background-image: url(' . $afbeelding . ')"></div>';
                            echo    '<div class="content center">';
                            echo    '<h4>' . $title . '</h4>';
                            echo    $tekst;
                            echo    '<div class="button btn">Lees bericht</div>';
                            echo    '</div>';
                            echo    '</a>';
                            echo    '</div>';
                            echo    '</div>';
                            
                        endwhile;
                        } wp_reset_query();
                        ?>
                         
                     </div>

                 </div>
             </div>
             
        </div>
    </section>

</main>
    
<?php
endwhile;
endif;
get_footer(); ?>