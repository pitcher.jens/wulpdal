<?php
$opmaak = get_sub_field('opmaak');
$titel = get_sub_field('titel');
$subtitel = get_sub_field('subtitel');
$tekst = get_sub_field('tekst');
if( get_sub_field('knop') ):
    $knop = get_sub_field('knop');
endif;

if ($opmaak == 'cta1') { ?>
    <section id="rendement" class="grey-bg component-bg">
        <div class="container">

            <div id="text-block" class="row">
                <div class="wrap center">
                    <div class="col">
                        <div class="divider"></div>
                        <div class="title">
                            <h2><span><?php echo $titel; ?></span></h2>

                            <h3><?php echo $subtitel; ?></h3>
                        </div>
                    </div>
                    <div class="col-12 col-lg-8 offset-lg-2">
                        <div class="content">
                            <?php echo $tekst; ?>
                        </div>
                        <a href="<?php echo $knop[url]; ?>" class="btn-secondary white btn" target="<?php echo $knop[target]; ?>"><?php echo $knop[title]; ?></a>
                    </div>
                </div>
            </div>

            <div class="component-1"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/wulpdal-component_schelp_2.png" alt=""></div>
        </div>

    </section>
<?php } else if ($opmaak == 'cta2') { ?>
    <section id="omgeving" class="grey-bg bg-image-bottom">

        <div class="container">
            <div id="text-block" class="row">
                <div class="wrap center">
                    <div class="col">
                        <div class="divider"></div>
                        <div class="title">
                            <h2><span><?php echo $titel; ?></span></h2>

                            <h3><?php echo $subtitel; ?></h3>
                        </div>
                    </div>
                    <div class="col-12 col-lg-8 offset-lg-2">
                        <div class="content">
                            <?php echo $tekst; ?>
                        </div>
                        <a href="<?php echo $knop[url]; ?>" class="btn-secondary white btn" target="<?php echo $knop[target]; ?>"><?php echo $knop[title]; ?></a>
                    </div>
                </div>
            </div>
        </div>
        
    </section>
<?php } ?>